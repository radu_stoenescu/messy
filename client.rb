require 'socket'
require 'yaml'
require_relative 'data_exchange'

client = TCPSocket.new('127.0.0.1', 6000)
client.print YAML::dump(DataExchange::Message.new)
client.close