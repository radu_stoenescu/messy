module DataExchange

  class Message
  
    def initialize  (type= :GREETINGS,
                     direction = :TO_CLIENT,
                     sender_id = :SERVER,
                     credentials = :NONE,
                     status = :OK,
                     *params)
      
      @type = type
      @direction = direction
      @sender_id = sender_id
      @credentials = credentials
      @params = params
    
    end
    
    attr_reader :type, :direction, :sender_id, :params, :credentials
  
    def to_s
    
      "Message from #@sender_id with params: #@params"
    
    end
    
  end

end