require 'gserver'
require 'yaml'
require 'yaml/store'
require_relative 'data_exchange'

module Server

  DEFAULT_DATA_STORE_FILE = "user_local_store.yml"
  
  class UserDataStore    
    def initialize(path_to_yaml = DEFAULT_DATA_STORE_FILE)
      @persisted = YAML::Store.new(path_to_yaml)
    end
    
    def add_client(client)
      @persisted.transaction do
        @persisted[client.username] = client.password
      end
    end
    
    def add_clients(clients)
      clients.each { |client| add_client(client) }
    end
    
    def remove_client(client)
      @persisted.transaction do
        @persisted[client.username] = nil
      end
    end
    
    def remove_clients(clients)
      clients.each { |client| remove_client(client) }
    end
        
    def check(username, password = nil)
      @persisted.transaction do
        case password
          when nil then @persisted[username] == nil
          else @persisted[username] == password
        end
      end
    end   
  end
  
  

  class Client
    
    def initialize(username, password, port, state = :OFFLINE)
      @username = username
      @password = password
      @port = port
      @state = state
    end
    
    attr_reader :username
    attr_accessor :password, :port, :state
    
    alias online? state
    
  end
  
  class BaseServer < GServer
  
    #def initialize(*args)
    #  super
    #  @clients = []
    #  @data_store = UserDataStore.new
    #end
  
    def serve(io)
      begin 
        request = YAML::load(io.read)
        p request
      rescue => e
        print e
      end
    end
  
  end
  
  b = BaseServer.new(6000)
  b.start.join
end